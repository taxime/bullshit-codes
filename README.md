![bullshit](resource/仓库内头图（900*100）.png)

时隔四年，bullshit-codes 仓库再次面向所有开发者征集你们遇到的坑爹代码！

这四年里，Gitee 见证了各种技术栈的更迭，陪伴了无数开发者的技术成长之路，也看到越来越多的优秀开源项目被人们所熟知。

当然，我们也相信，这四年里出现过多少优雅的代码，就出现过多少坑爹的代码。

我们坚信：The most bullshit codes are yet to come，把那些让你哭笑不得、怀疑人生、血压升高的代码展示出来吧，庆祝我们的错误，让我们一起从中学习，共同提升。

### 坑爹代码2.0有奖征集活动现已结束，仓库仍将继续公开并接受 PR。


## 「还！有！谁！！！」奖

@Lv丶小胖：[这绝对是世上最快的提醒商家发货功能](https://gitee.com/gitee-community/bullshit-codes/pulls/267)

## 「求求你转行吧」奖

@polarloves

## 「公开处刑」名单

- 7月20日「公开处刑」人员

@yufire：[我就修改一下我的Bean，很合理吧，为什么系统崩了？呜呜呜~](https://gitee.com/gitee-community/bullshit-codes/pulls/383)

@polarloves：[域名判断代码](https://gitee.com/gitee-community/bullshit-codes/pulls/386/files)


- 7月19日「公开处刑」人员

@polarloves：[他尽力了，我无话可说](https://gitee.com/gitee-community/bullshit-codes/pulls/360)

- 7月18日「公开处刑」人员

@min0911：[世界上最快的汇编器](https://gitee.com/gitee-community/bullshit-codes/pulls/330/files)

- 7月17日「公开处刑」人员

@绫织梦：[高中时期，看同学写的奇葩代码里起的逆天缩写类名](https://gitee.com/gitee-community/bullshit-codes/pulls/308)

@杰里：[一个sql查只一个字段。满满的工作量](https://gitee.com/gitee-community/bullshit-codes/pulls/311)

@呀吖呀吖呀：[将dto中的type转换为时间的骚操作](https://gitee.com/gitee-community/bullshit-codes/pulls/326)

- 7月14日「公开处刑」人员

@lzy6666666：[公司高级JAVA工程师的业务代码，高！实在是高！](https://gitee.com/gitee-community/bullshit-codes/pulls/288)


- 7月13日「公开处刑」人员

@伊成：[为了应付公司每月统计代码行数，同事的写法也是醉了...](https://gitee.com/gitee-community/bullshit-codes/pulls/273)

@hishuwei：[winform程序路径写log文件](https://gitee.com/gitee-community/bullshit-codes/pulls/272)

@Lv丶小胖：[这绝对是世上最快的提醒商家发货功能](https://gitee.com/gitee-community/bullshit-codes/pulls/267)

- 7月12日「公开处刑」人员

@卖兔子的小萝卜：[架构怒发冲冠的对我说不懂JVM的机制](https://gitee.com/gitee-community/bullshit-codes/pulls/258)

@sahooz：[论单机无联网App如何无痛升级数据库：升级数据库就删除所有表然后重建](https://gitee.com/gitee-community/bullshit-codes/pulls/250)


## 如何提交坑爹代码

1. Fork 本仓库，并创建一个包含坑爹代码的 Pull Request；或直接在对应语言的文件夹下上传/新建文件，创建一个轻量级 Pull Request。
2. Pull Request 必须包含清晰的标题和说明，描述为什么这段代码是坑爹代码。
3. 请给提交的文件取一个有意义的文件名及符合对应语言的文件后缀名，描述代码相关信息可以使用注释。具体形式可参考[Demo.java](java/Demo.java)。
4. Gitee 团队会审核这些 Pull Requests，接受符合要求的提交。

## 选出你心目中的坑爹代码
1. 进入[已合并的 Pull Requests 列表](https://gitee.com/gitee-community/bullshit-codes/pulls?assignee_id=&author_id=&label_ids=&label_text=&milestone_id=&priority=&project_id=gitee-community%2Fbullshit-codes&project_type=&scope=&search=&single_label_id=&single_label_text=&sort=closed_at+desc&status=merged&target_project=&tester_id=)中为你心中的坑爹代码评论及点赞。
2. 每天被合并 PR 中累计 **评论人数+点赞次数** 最多的 PR 将会获得当天的「公开处刑」资格，并在活动期间工作日的 18:00 正式公布。

以下图 PR 为例，评论人数+点赞次数为 2：

![](resource/eg.jpg)

## 评选规则

- 本次坑爹代码 2.0 收集活动日期为：2023年7月11日至2023年7月21日。
- 活动期间工作日的 18:00 后提交的 PR 将参与次日的评选，周六及周日提交的 PR 将参与周一的评选。
- 本次活动不限制编程语言，不限制提交次数。
- 参赛的代码至少是可运行的代码或者是看似可运行的代码。
- 所提交代码不包含商业及政治敏感信息，需匿名化作者和公司。

## 奖项设置

### 「精神抚慰」奖

 每天被「公开处刑」的提交者将会获得「精神抚慰」奖 ： **技术书籍/马建仓公仔/开源内裤/Gitee 定制马克杯** 任选其一。

### 「求求你转行吧」奖

活动期间被合并PR数量最多的开发者将会获得「求求你转行吧」奖：**技术书籍/马建仓公仔/开源内裤/Gitee 定制马克杯** 任选其三。

### 「还！有！谁！！！」奖

活动期间累计获得 **评论人数+点赞次数** 最多的 PR，该 PR 的提交者将会获得「还！有！谁！！！」奖：**技术书籍+马建仓公仔+开源内裤+Gitee 定制马克杯** 。


## 奖品一览

### 技术书籍

![](resource/book.png)

### 马建仓公仔

![](resource/mjc.png)

### 开源内裤

![](resource/underwear.png)

### Gitee 定制马克杯

![](resource/cup.png)
