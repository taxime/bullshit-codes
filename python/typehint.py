"""
这是以前碰到的一个神奇的问题抽象出来的demo，现在也不是很清楚为什么
下面给出来的代码是可以跑的
然而一旦把a=1换为注释的那段代码就会报SyntaxError
仅仅只是加了一个type hint而已啊
离谱
"""

"""
PEP-526对此进行了解释：
在同一个函数作用域中尝试注解受global或nonlocal约束的变量是非法的：

def f():
    global x: int  # SyntaxError
def g():
    x: int  # Also a SyntaxError
    global x
    
原因是global和nonlocal不拥有变量;因此，类型注解属于拥有该变量的作用域。
解决方案是在函数外部（即在全局命名空间中）注解变量，并删除函数范围内的类型注解
"""

a = None

def global_init():
    global a
    a = 1
    #a:int = 1

global_init()
print(a)