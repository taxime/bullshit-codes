#include <iostream>

int main() {
    // this exception may happen when we use the log to print array
    // In asan, we will get the exception
    char foo[5] = {'H','E','L','L','O'};
    printf("%s\n", foo);
}