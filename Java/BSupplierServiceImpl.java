
// 明明只是按需求过滤状态，怎么查询就永远只有10条记录呢
public TableDataInfo<BSupplierVo> selectPage(BSupplier query, PageQuery pageQuery) {
    Page<BlessSupplierVo> page = baseMapper.selectPage(pageQuery.build(), wrapper(query));
    TableDataInfo<BlessSupplierVo> dataInfo = TableDataInfo.build(page);
    List<BlessSupplierVo> rows = dataInfo.getRows();
    if (CollUtil.isNotEmpty(rows)) {
        // 如果是状态0 就把不是0的干掉
        if ("0".equals(query.getMonthRt())){
            rows.removeIf(e -> !"0".equals(e.getMonthRt()));
            // 如果是状态1 就把不是1的干掉
        }else if ("1".equals(query.getMonthRt())){
            rows.removeIf(e -> !"1".equals(e.getMonthRt()));
        }
        dataInfo.setRows(rows);
        dataInfo.setTotal(rows.size());
    }
    return dataInfo;
}