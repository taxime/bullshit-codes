public class XXXServiceImpl implements XXXService {

    // 某团队的规范
    // 当你想使用缓存的时候，不允许在这里加@Cacheable，必须新增一个cache层
    @Override
    public XXX xxx(String id) {
        return xxxService.xxx(id);
    }
}




public class XXXCacheServiceImpl implements XXXCacheService {

    @Autowired
    private  XXXService xxxService;

    @Override
    @Cacheable(value = "auth", key = "targetClass.name+'_'+#id", unless = "#result eq null")
    public XXX xxx(String id) {
        return xxxService.xxx(id);
    }
}
